package com.webflux.demo.model;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document("employees")
public class Employee {

  private String id;
  @Field("EMPLOYEE_ID")
  private Integer employeeId;
  @Field("FIRST_NAME")
  private String firstName;
  @Field("LAST_NAME")
  private String lastName;
  @Field("EMAIL")
  private String email;
  @Field("PHONE_NUMBER")
  private String phoneNumber;
  @Field("HIRE_DATE")
  private String hireDate;
  @Field("SALARY")
  private Integer salary;
  @Field("DEPARTMENT_ID")
  private Integer departmentId;

}
