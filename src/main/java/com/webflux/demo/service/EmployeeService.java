package com.webflux.demo.service;

import com.webflux.demo.model.Employee;
import reactor.core.publisher.Mono;

public interface EmployeeService {

  Mono<Employee> findEmployee(String id);

}
