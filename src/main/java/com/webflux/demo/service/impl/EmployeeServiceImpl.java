package com.webflux.demo.service.impl;

import com.webflux.demo.model.Employee;
import com.webflux.demo.repository.EmployeeRepository;
import com.webflux.demo.service.EmployeeService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@AllArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {

  private final EmployeeRepository employeeRepository;

  @Override
  public Mono<Employee> findEmployee(String id) {

    return employeeRepository.findById(id);
  }
}
