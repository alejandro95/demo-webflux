package com.webflux.demo.controller;

import com.webflux.demo.model.Employee;
import com.webflux.demo.service.EmployeeService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/employee")
@AllArgsConstructor
public class EmployeeController {

  private final EmployeeService employeeService;


  @GetMapping("/{id}")
  public Mono<Employee> findEmployeeById(@PathVariable String id) {
    return employeeService.findEmployee(id)
        .doOnSuccess(employee ->
            System.out.println(employee));
  }

}
